fun main() {
    val p1 = Point(7, 3)
    val p2 = Point(2, 8)
    println(p1.toString())
    println(p2.toString())
    println(p1.equals(p2))
    println("simetriuli" + -p1)
    println("simetriuli" + -p2)

}

data class Point(val x:Int, val y:Int) {
    override fun toString(): String {
        return "$x, $y"
    }


    override fun equals(on: Any?): Boolean {
        if (on is Point) {
            return (x == on.x) && (y == on.y)
        }
        return false
    }
}
operator fun Point.unaryMinus() = Point(-x, -y)